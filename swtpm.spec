%bcond_without gnutls

# Macros needed by SELinux
%global selinuxtype targeted
%global moduletype  contrib
%global modulename  swtpm

Summary: TPM Emulator
Name:           swtpm
Version:        0.8.0
Release:        3
License:        BSD
Url:            https://github.com/stefanberger/swtpm
Source0:        %{url}/archive/v%{version}/%{name}-%{version}.tar.gz
Patch00:        0000-disable-test_tpm2_swtpm_localca_pkcs11.test.patch
Patch01:        0001-swtpm_setup-Initialized-argv-in-get_swtpm_capabiliti.patch

BuildRequires:  automake
BuildRequires:  autoconf
BuildRequires:  libtool
BuildRequires:  libtpms-devel >= 0.6.0
BuildRequires:  glib2-devel
BuildRequires:  json-glib-devel
BuildRequires:  expect
BuildRequires:  net-tools
BuildRequires:  openssl-devel
BuildRequires:  socat
BuildRequires:  softhsm
BuildRequires:  trousers >= 0.3.9
%if %{with gnutls}
BuildRequires:  gnutls >= 3.1.0
BuildRequires:  gnutls-devel
BuildRequires:  gnutls-utils
BuildRequires:  libtasn1-devel
BuildRequires:  libtasn1
%endif
BuildRequires:  selinux-policy-devel
BuildRequires:  gcc
BuildRequires:  libseccomp-devel
BuildRequires:  tpm2-tools
BuildRequires:  chrpath

Requires:       %{name}-libs = %{version}-%{release}
Requires:       libtpms >= 0.6.0
%{?selinux_requires}

%description
TPM emulator built on libtpms providing TPM functionality for QEMU VMs

%package        libs
Summary:        Private libraries for swtpm TPM emulators
License:        BSD

%description    libs
A private library with callback functions for libtpms based swtpm TPM emulator

%package        devel
Summary:        Include files for the TPM emulator's CUSE interface for usage by clients
License:        BSD
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}

%description    devel
Include files for the TPM emulator's CUSE interface.

%package        tools
Summary:        Tools for the TPM emulator
License:        BSD
Requires:       swtpm = %{version}-%{release}
Requires:       trousers >= 0.3.9 bash gnutls-utils

%description    tools
Tools for the TPM emulator from the swtpm package

%prep
%autosetup -n %{name}-%{version} -p1
%selinux_relabel_pre -s %{selinuxtype}

%build

NOCONFIGURE=1 ./autogen.sh
%configure \
%if %{with gnutls}
        --with-gnutls \
%endif
        --without-cuse

%make_build

%check
make %{?_smp_mflags} check

%install

%make_install
rm -f $RPM_BUILD_ROOT%{_libdir}/%{name}/*.{a,la,so}
rm -f $RPM_BUILD_ROOT%{_mandir}/man8/swtpm-create-tpmca.8*
rm -f $RPM_BUILD_ROOT%{_datadir}/%{name}/swtpm-create-tpmca

chrpath -d %{buildroot}/%{_bindir}/swtpm
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/%{name}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%post
for pp in /usr/share/selinux/packages/swtpm.pp \
          /usr/share/selinux/packages/swtpm_svirt.pp; do
  %selinux_modules_install -s %{selinuxtype} ${pp}
done

/sbin/ldconfig

%postun
if [ $1 -eq  0 ]; then
  for p in swtpm swtpm_svirt; do
    %selinux_modules_uninstall -s %{selinuxtype} $p
  done
fi

%posttrans
%selinux_relabel_post -s %{selinuxtype}

%ldconfig_post libs
%ldconfig_postun libs

/sbin/ldconfig

%files
%license LICENSE
%doc README
%{_bindir}/swtpm
%{_mandir}/man8/swtpm.8*
%{_datadir}/selinux/packages/swtpm.pp
%{_datadir}/selinux/packages/swtpm_svirt.pp
%config(noreplace) /etc/ld.so.conf.d/*

%files libs
%license LICENSE
%doc README

%dir %{_libdir}/%{name}
%{_libdir}/%{name}/libswtpm_libtpms.so.0
%{_libdir}/%{name}/libswtpm_libtpms.so.0.0.0

%files devel
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/*.h
%{_mandir}/man3/swtpm_ioctls.3*

%files tools
%doc README
%{_bindir}/swtpm_bios
%if %{with gnutls}
%{_bindir}/swtpm_cert
%endif
%{_bindir}/swtpm_setup
%{_bindir}/swtpm_ioctl
%{_bindir}/swtpm_localca
%{_mandir}/man8/swtpm_bios.8*
%{_mandir}/man8/swtpm_cert.8*
%{_mandir}/man8/swtpm_cuse.8*
%{_mandir}/man8/swtpm_ioctl.8*
%{_mandir}/man5/swtpm-localca.conf.5*
%{_mandir}/man5/swtpm-localca.options.5*
%{_mandir}/man8/swtpm-localca.8*
%{_mandir}/man8/swtpm_localca.8*
%{_mandir}/man8/swtpm_setup.8*
%{_mandir}/man5/swtpm_setup.conf.5*
%config(noreplace) %{_sysconfdir}/swtpm_setup.conf
%config(noreplace) %{_sysconfdir}/swtpm-localca.options
%config(noreplace) %{_sysconfdir}/swtpm-localca.conf
%dir %{_datadir}/swtpm
%{_datadir}/swtpm/swtpm-localca
%{_datadir}/swtpm/swtpm-create-user-config-files
%attr( 750, tss, root) %{_localstatedir}/lib/swtpm-localca

%changelog
* Mon Aug 28 2023 Jiabo Feng <fengjiabo1@huawei.com> - 0.8.0-3
- swtpm_setup: Initialized @argv in get_swtpm_capabilities()

* Tue Mar 07 2023 jiangfangjie <jiangfangjie@huawei.com> - 0.8.0-2
- Remove rpath

* Fri Feb 03 2023 yezengruan <yezengruan@huawei.com> - 0.8.0-1
- update to version 0.8.0

* Wed Nov 23 2022 yezengruan <yezengruan@huawei.com> - 0.3.3-7
- Update the source package to be consistent with the download link

* Thu Jun 30 2022 yezengruan <yezengruan@huawei.com> - 0.3.3-6
- Addressed potential symlink attack issue (CVE-2020-28407)

* Wed Mar 9 2022 yaoxin <yaoxin30@huawei.com> - 0.3.3-5
- swtpm: Check header size indicator against expected size (CVE-2022-23645)

* Wed Feb 16 2022 xu_ping <xuping33@huawei.com> - 0.3.3-4
- rename deprecated libtasn1 types to fix build error

* Tue Nov 16 2021 imxcc <xingchaochao@huawei.com> - 0.3.3-3
- add selinux_relabel_pre

* Tue Sep 15 2020 jiangfangjie <jiangfangjie@huawei.com> - 0.3.3-2
- update spec file and disable test case pkcs11

* Mon Aug 24 2020 jiangfangjie <jiangfangjie@huawei.com> - 0.3.3-1
- Created initial version of rpm spec files
- Version is now 0.3.3
